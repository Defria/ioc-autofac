using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Core;

namespace Hydra.IoC
{
    public static class Registration
    {
        public static IContainer Register()
        {
            var logger = CreateLogger();
            var serviceCollection = RegisterServiceCollection(logger);
            var assembly = Assembly.Load(nameof(Hydra));
            var interfaces = assembly.GetTypes().Where(t => t.IsInterface);

            var builder = new ContainerBuilder();
            builder.Populate(serviceCollection);
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.IsClass && interfaces.Any(i => i.Name.Contains(t.Name)))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name.Contains(t.Name) && i.IsInterface)).AsSelf();

            logger.Information("I'm up");
            var container = builder.Build();
            return container;
        }

        private static ServiceCollection RegisterServiceCollection(ILogger logger)
        {
            var services = new ServiceCollection();
            services.AddLogging(l => l.AddSerilog(logger));
            return services;
        }

        private static Logger CreateLogger()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
                .Build();

            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
            return logger;
        }
    }
}